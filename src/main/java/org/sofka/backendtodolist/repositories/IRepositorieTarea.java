package org.sofka.backendtodolist.repositories;


import org.sofka.backendtodolist.models.Tareas;
import org.springframework.data.repository.CrudRepository;

public interface IRepositorieTarea extends CrudRepository<Tareas, Integer> {
}
