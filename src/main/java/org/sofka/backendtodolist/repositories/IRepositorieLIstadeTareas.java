package org.sofka.backendtodolist.repositories;

import org.sofka.backendtodolist.models.ListadeTareas;
import org.sofka.backendtodolist.models.Tareas;

import java.util.ArrayList;

public interface IRepositorieLIstadeTareas {
    public ArrayList<ListadeTareas> getListas();

    public ArrayList<Tareas> getTareas();

    public ListadeTareas GuardarLista(ListadeTareas listadeTareas);

    public Tareas GuardarTarea(Tareas tareas);

    public Boolean EliminarLista(Integer id);

    public Boolean EliminarTarea(Integer id);

    public Tareas ActualizarTarea(Integer id, Tareas tareas);

}

