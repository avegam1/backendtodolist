package org.sofka.backendtodolist.repositories;

import org.sofka.backendtodolist.models.ListadeTareas;
import org.springframework.data.repository.CrudRepository;

public interface IRepositorieLista extends CrudRepository<ListadeTareas, Integer> {
}
