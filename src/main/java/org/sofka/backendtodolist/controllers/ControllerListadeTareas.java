package org.sofka.backendtodolist.controllers;

import org.sofka.backendtodolist.models.ListadeTareas;
import org.sofka.backendtodolist.models.Tareas;
import org.sofka.backendtodolist.service.ServiceTodolist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin("*")
@RestController
@RequestMapping("/")
public class ControllerListadeTareas {
    @Autowired
 ServiceTodolist serviceTodolist;

    @GetMapping("/listadetareas")
    public ArrayList<ListadeTareas> getLists(){
        return serviceTodolist.getListas();
    }

    @PostMapping("/listadetareas")
    public ListadeTareas saveListing(@RequestBody ListadeTareas listing){
        return this.serviceTodolist.GuardarLista(listing);
    }

    @GetMapping("/tareas")
    public ArrayList<Tareas> getTasks(){
        return serviceTodolist.getTareas();
    }

    @PostMapping("/tareas")
    public Tareas guardartarea(@RequestBody Tareas tareas){

        return this.serviceTodolist.GuardarTarea(tareas);
    }

    @DeleteMapping(path = "/listadetareas/{id}")
    public String eliminarlista(@PathVariable("id") Integer id){
        boolean ok = this.serviceTodolist.EliminarLista(id);
        if (ok) {
            return "Se eliminó la lista con id " + id;
        } else {
            return "No se pudo eliminar la lista con id " + id;
        }
    }
    @DeleteMapping(path = "/tareas/{id}")
    public String eliminartarea(@PathVariable("id") Integer id){
        boolean ok = this.serviceTodolist.EliminarTarea(id);
        if (ok) {
            return "Se eliminó la tarea con id " + id;
        } else {
            return "No se pudo eliminar la tarea con id " + id;
        }
    }

    @PutMapping(path = "/tareas/{id}")
    public Tareas actulizartarea(@PathVariable("id") Integer id, @RequestBody Tareas tareas){
        return serviceTodolist.ActualizarTarea(id, tareas);
    }


}
