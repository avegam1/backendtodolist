package org.sofka.backendtodolist.service;

import org.sofka.backendtodolist.models.ListadeTareas;
import org.sofka.backendtodolist.models.Tareas;
import org.sofka.backendtodolist.repositories.IRepositorieLIstadeTareas;
import org.sofka.backendtodolist.repositories.IRepositorieLista;
import org.sofka.backendtodolist.repositories.IRepositorieTarea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@Service
public class ServiceTodolist implements IRepositorieLIstadeTareas {
    @Autowired
    IRepositorieLista repositorieLista;
    @Autowired
    IRepositorieTarea repositorieTarea;

    @Override
    public ArrayList<ListadeTareas> getListas() {
        return (ArrayList<ListadeTareas>) repositorieLista.findAll();
    }

    @Override
    public ArrayList<Tareas> getTareas() {
        return (ArrayList<Tareas>) repositorieTarea.findAll();
    }

    @Override
    public ListadeTareas GuardarLista(ListadeTareas listing) {
        return repositorieLista.save(listing);
    }

    @Override
    public Tareas GuardarTarea(Tareas tareas) {
        tareas.setCompleted(false);
        return repositorieTarea.save(tareas);
    }

    @Override
    public Boolean EliminarLista(Integer id) {
        try {
            repositorieLista.deleteById(id);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public Boolean EliminarTarea(Integer id) {
        try {
            repositorieTarea.deleteById(id);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public Tareas ActualizarTarea(Integer id, Tareas tareas) {
        tareas.setId(id);
        return repositorieTarea.save(tareas);
    }
}
